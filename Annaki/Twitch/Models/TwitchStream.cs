﻿namespace Annaki.Twitch.Models
{
    public class TwitchStream
    {
        public string Title { get; set; }
        public string StreamerDisplayName { get; set; }
        public string StreamerId { get; set; }
        public string ProfileUrl { get; set; }
        public string Game { get; set; }
        public string BoxArtUrl { get; set; }
        public string Url => $"https://www.twitch.tv/{this.StreamerDisplayName}";
        public string PreviewUrl => $"https://static-cdn.jtvnw.net/previews-ttv/live_user_{StreamerDisplayName.ToLower()}-1920x1080.jpg?rnd={new Random().Next()}";
    }
}
