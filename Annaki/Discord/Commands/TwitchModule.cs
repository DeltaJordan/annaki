﻿using Annaki.Discord.Exceptions;
using Annaki.Twitch;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using NLog;

namespace Annaki.Discord.Commands
{
    [SlashCommandGroup("ttv", "Commands related to twitch announcements.")]
    public class TwitchModule : ApplicationCommandModule
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [SlashCommand("add", "Adds announcments to the indicated channel.")] 
        [SlashRequireUserPermissions(DSharpPlus.Permissions.Administrator)]
        public async Task AddStreamerAsync(
            InteractionContext ctx,
            [Option("channel", "The channel to post the go live announcement.", true)] DiscordChannel channel,
            [Option("username", "Twitch username of the channel to announce when live.")] string username)
        {
            try
            {
                await Program.TwitchClient.AddNotificationAsync(username, ctx.Guild.Id, channel.Id).SafeAsync();

                await ctx.CreateResponseAsync($"Successfully added notifications for {username} to {channel.Mention}.", true).SafeAsync();
            }
            catch (AnnakiDuplicateException)
            {
                await ctx.CreateResponseAsync("This streamer already has notifications in this server. Duplicate notifications are not supported.", true).SafeAsync();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        [SlashCommand("remove", "Removes all announcments for the user in the server.")]
        [SlashRequireUserPermissions(DSharpPlus.Permissions.Administrator)]
        public async Task RemoveStreamerAsync(
            InteractionContext ctx,
            [Option("username", "Twitch username of the channel to announce when live.")] string username
        ) {
            try
            {
                if (await Program.TwitchClient.RemoveNotificationAsync(username, ctx.Guild.Id).SafeAsync())
                {
                    await ctx.CreateResponseAsync($"Successfully removed notifications for {username}.", true).SafeAsync();
                }
                else
                {
                    await ctx.CreateResponseAsync("This server does not have any notifications enabled for this streamer!", true).SafeAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }

        [SlashCommand("notification", "Adds or removes the notification ping from yourself.")]
        public async Task ToggleNotifRoleAsync(InteractionContext ctx)
        {
            DiscordRole notifRole = ctx.Guild.Roles
                .Select(x => x.Value)
                .FirstOrDefault(x => x.Name.Equals("notifications", StringComparison.InvariantCultureIgnoreCase));

            if (notifRole != null)
            {
                if (ctx.Member.Roles.Any(x => x.Id == notifRole.Id))
                {
                    await ctx.Member.RevokeRoleAsync(notifRole).SafeAsync();
                    await ctx.CreateResponseAsync("You will no longer recieve notifications in this server.", true).SafeAsync();
                }
                else
                {
                    await ctx.Member.GrantRoleAsync(notifRole).SafeAsync();
                    await ctx.CreateResponseAsync("You will now recieve notifications.", true).SafeAsync();
                }
            }
        }
    }
}