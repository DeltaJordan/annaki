﻿using DSharpPlus.Lavalink;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annaki.Discord.Commands.Containers
{
    public static class MusicContainer
    {
        public static readonly ConcurrentDictionary<ulong, List<LavalinkTrack>> Queue = new();
        public static readonly ConcurrentDictionary<ulong, List<LavalinkTrack>> Stale = new();
        public static readonly ConcurrentDictionary<ulong, bool> Loop = new();
    }
}
