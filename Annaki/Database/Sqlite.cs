﻿using Annaki.IO;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annaki.Database
{
    public static class Sqlite
    {
        private const string INIT_SQL = @"
PRAGMA encoding = ""UTF-8"";

CREATE TABLE IF NOT EXISTS STREAMERS(
    id INTEGER PRIMARY KEY,
    twitch_user_id TEXT NOT NULL,
    guild_id INTEGER NOT NULL,
    channel_id INTEGER NOT NULL
);";

        public static async Task<SqliteConnection> CreateAsync(CancellationToken cancellationToken = default)
        {
            string dbFile = Path.Combine(IOUtil.AppDirectory, "annaki.sqlite");

            SqliteConnectionStringBuilder connectionStringBuilder = new();
            connectionStringBuilder.DataSource = dbFile;
            connectionStringBuilder.Mode = SqliteOpenMode.ReadWriteCreate;
            SqliteConnection db = new(connectionStringBuilder.ConnectionString);
            await db.OpenAsync(cancellationToken).SafeAsync();

            SqliteCommand command = db.CreateCommand();
            command.CommandText = INIT_SQL;
            command.ExecuteNonQuery();

            return db;
        }
    }
}
