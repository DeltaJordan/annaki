﻿using Annaki.Database;
using Annaki.Logging;
using Annaki.Security;
using Annaki.Twitch.Models;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using DSharpPlus.SlashCommands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Annaki.Discord
{
    public class AnnakiDiscordClient
    {
        public DiscordClient InternalClient { get; }

        private readonly LavalinkConfiguration lavalinkConfig;
        private readonly LavalinkExtension lavalink;

        public AnnakiDiscordClient()
        {
            ApiCredentials credentials = new("discord");
            if (string.IsNullOrWhiteSpace(credentials.Key))
            {
                Console.WriteLine("Please enter Discord api key: ");

                // Keep the key from being in console history.
                string discordKey = string.Empty;
                while (true)
                {
                    var input = System.Console.ReadKey(true);
                    if (input.Key == ConsoleKey.Enter)
                        break;
                    discordKey += input.KeyChar;
                }

                credentials.Key = discordKey;
                credentials.Save();
            }

            DiscordConfiguration discordConfig = new()
            {
                Token = credentials.Key,
                Intents = DiscordIntents.Guilds | DiscordIntents.MessageContents
            };
            InternalClient = new DiscordClient(discordConfig);

            SlashCommandsConfiguration slashCommandsConfiguration = new();

            SlashCommandsExtension commands = InternalClient.UseSlashCommands(slashCommandsConfiguration);
            commands.RegisterCommands(Assembly.GetExecutingAssembly());

            Program.TwitchClient.Live += TwitchClient_LiveAsync;
            InternalClient.MessageCreated += InternalClient_MessageCreatedAsync;

            var endpoint = new ConnectionEndpoint
            {
                Hostname = "127.0.0.1",
                Port = 2333
            };

            lavalinkConfig = new LavalinkConfiguration
            {
                Password = "youshallnotpass",
                RestEndpoint = endpoint,
                SocketEndpoint = endpoint
            };

            lavalink = InternalClient.UseLavalink();
            InternalClient.UseInteractivity(new InteractivityConfiguration
            {
                PollBehaviour = PollBehaviour.DeleteEmojis,
                Timeout = TimeSpan.FromSeconds(30)
            });
        }

        public async Task StartAsync()
        {
            await InternalClient.ConnectAsync().SafeAsync();
            await lavalink.ConnectAsync(lavalinkConfig).SafeAsync();
        }

        private async Task InternalClient_MessageCreatedAsync(DiscordClient sender, DSharpPlus.EventArgs.MessageCreateEventArgs e)
        {
            if (e.Guild.Id == 738823408686727248 || e.Guild.Id == 1006947253530939462)
            {
                DiscordMember author = await e.Guild.GetMemberAsync(e.Author.Id).SafeAsync();

                string foundTwitter = e.Message.Content.Split(' ')
                    .FirstOrDefault(x => 
                        x.Contains("twitter.com/", StringComparison.InvariantCultureIgnoreCase)
                        && x.Contains("status", StringComparison.InvariantCultureIgnoreCase)
                        && !x.Contains("vxtwitter.com/", StringComparison.InvariantCultureIgnoreCase)
                        && Uri.TryCreate(x, UriKind.Absolute, out _)
                    );

                if (foundTwitter != null)
                {
                    string vxDomain = e.Message.Embeds.Any(x => x.Image != null) ? "c.vxtwitter.com/" : "vxtwitter.com/";
                    string result = e.Message.Content.Replace(foundTwitter, foundTwitter.Replace("twitter.com/", vxDomain));
                    await e.Channel.SendMessageAsync($"__**{author.DisplayName}:**__\n{result}").SafeAsync();
                    await e.Message.DeleteAsync().SafeAsync();
                }
            }
        }

        private async void TwitchClient_LiveAsync(object sender, TwitchStream e)
        {
            DiscordEmbedBuilder builder = new()
            {
                Title = e.Title,
                Url = e.Url,
                ImageUrl = e.PreviewUrl,
                Thumbnail = new DiscordEmbedBuilder.EmbedThumbnail
                {
                    Height = 270,
                    Width = 270,
                    Url = e.ProfileUrl
                },
                Color = new DiscordColor(100, 65, 165)
            };

            builder.WithAuthor(e.StreamerDisplayName);
            builder.WithFooter($"Playing: {e.Game}", e.BoxArtUrl);

            TwitchNotificationDbo[] notificationDbos = await DataProvider.FetchNotificationsByStreamerIdAsync(e.StreamerId).SafeAsync();

            foreach (TwitchNotificationDbo notificationDbo in notificationDbos)
            { 
                DiscordChannel notifChannel = await InternalClient.GetChannelAsync(notificationDbo.ChannelId).SafeAsync();

                DiscordRole notifRole = notifChannel.Guild.Roles
                    .Select(x => x.Value)
                    .FirstOrDefault(x => x.Name.Equals("notifications", StringComparison.InvariantCultureIgnoreCase));

                string notifMessageContent = notifRole == null ?
                    $"@everyone {e.StreamerDisplayName} is now live!" :
                    $"{notifRole.Mention} {e.StreamerDisplayName} is now live!";

                await notifChannel.SendMessageAsync(notifMessageContent, builder.Build()).SafeAsync();
            }
        }
    }
}
