﻿using Annaki.IO;
using Newtonsoft.Json;

namespace Annaki.Core
{
    public class AppSettings
    {
        public ulong[] MusicWhitelist { get; set; }

        public AppSettings()
        {
        }

        public static async Task<AppSettings> LoadSettingsAsync()
        {
            string settingsPath = Path.Combine(IOUtil.AppDirectory, "settings.json");
            if (!File.Exists(settingsPath))
            {
                AppSettings settings = new()
                {
                    MusicWhitelist = new ulong[] { 228019100008316948 }
                };

                return settings;
            }

            return JsonConvert.DeserializeObject<AppSettings>(await File.ReadAllTextAsync(settingsPath).SafeAsync());
        }
    }
}
