﻿using Annaki.Database;
using Annaki.Discord.Exceptions;
using Annaki.Security;
using Annaki.Twitch.Models;
using DSharpPlus.Entities;
using TwitchLib.Api;
using TwitchLib.Api.Helix.Models.Games;
using TwitchLib.Api.Helix.Models.Users.GetUsers;
using TwitchLib.Api.Services;

namespace Annaki.Twitch
{
    public class AnnakiTwitchClient
    {
        public TwitchAPI InternalClient { get; }

        public event EventHandler<TwitchStream> Live;

        private readonly LiveStreamMonitorService liveStreamMonitorService;

        public AnnakiTwitchClient()
        {
            ApiCredentials credentials = new("twitch");
            if (string.IsNullOrWhiteSpace(credentials.Key))
            {
                Console.WriteLine("Please enter Twitch client id: ");
                string clientId = Console.ReadLine();

                Console.WriteLine("Please enter Twitch api key: ");

                // Keep the key from being in console history.
                string key = string.Empty;
                while (true)
                {
                    var input = System.Console.ReadKey(true);
                    if (input.Key == ConsoleKey.Enter)
                        break;
                    key += input.KeyChar;
                }

                credentials.Id = clientId;
                credentials.Key = key;
                credentials.Save();
            }

            InternalClient = new TwitchAPI();
            InternalClient.Settings.ClientId = credentials.Id;
            InternalClient.Settings.Secret = credentials.Key;

            liveStreamMonitorService = new LiveStreamMonitorService(InternalClient);
            liveStreamMonitorService.OnStreamOnline += LiveStreamMonitorService_OnStreamOnlineAsync;
        }

        public async Task StartAsync()
        {
            InternalClient.Settings.AccessToken = await InternalClient.Helix.Extensions.GetAccessTokenAsync().SafeAsync();

            List<string> streamers = await DataProvider.FetchAllStreamersAsync().SafeAsync();
            if (streamers.Count == 0)
                return;

            liveStreamMonitorService.SetChannelsById(streamers);
            liveStreamMonitorService.Start();
        }

        public async Task<string> GetUserIdAsync(string username)
        {
            GetUsersResponse response = await InternalClient.Helix.Users.GetUsersAsync(logins: new List<string> { username }).SafeAsync();
            return response.Users.First().Id;
        }

        public async Task AddNotificationAsync(string username, ulong guildId, ulong channelId)
        {
            string streamerId = await GetUserIdAsync(username).SafeAsync();

            TwitchNotificationDbo notificationDbo = await DataProvider.FetchNotificationAsync(streamerId, guildId).SafeAsync();
            if (notificationDbo?.GuildId == guildId)
            {
                throw new AnnakiDuplicateException("Cannot create duplicate notifications for a streamer in the same guild.");
            }

            await DataProvider.AddNotificationAsync(streamerId, guildId, channelId).SafeAsync();
            await UpdateNotificationsAsync().SafeAsync();
        }

        public async Task<bool> RemoveNotificationAsync(string username, ulong guildId)
        {
            string streamerId = await GetUserIdAsync(username).SafeAsync();
            
            if (!await DataProvider.RemoveNotificationsByLocationAsync(streamerId, guildId).SafeAsync())
            {
                return false;
            }
            
            await UpdateNotificationsAsync().SafeAsync();

            return true;
        }

        private async Task UpdateNotificationsAsync()
        {
            List<string> streamers = await DataProvider.FetchAllStreamersAsync().SafeAsync();

            if (streamers.Count == 0)
            {
                if (liveStreamMonitorService.Enabled)
                    liveStreamMonitorService.Stop();

                // Cannot set an empty list. Should be fine though, since add a user replaces whatever list was here.
                // liveStreamMonitorService.SetChannelsById(streamers);
                return;
            }

            liveStreamMonitorService.SetChannelsById(streamers);

            if (!liveStreamMonitorService.Enabled)
            {
                liveStreamMonitorService.Start();
            }
        }

        private async void LiveStreamMonitorService_OnStreamOnlineAsync(object sender, TwitchLib.Api.Services.Events.LiveStreamMonitor.OnStreamOnlineArgs e)
        {
            GetUsersResponse usersResponse = await InternalClient.Helix.Users.GetUsersAsync(new List<string> { e.Stream.UserId }).SafeAsync();
            User streamer = usersResponse.Users.First();
            GetGamesResponse gamesResponse = await InternalClient.Helix.Games.GetGamesAsync(new List<string> { e.Stream.GameId }).SafeAsync();
            Game game = gamesResponse.Games.First();

            TwitchStream twitchStream = new()
            {
                Title = e.Stream.Title,
                ProfileUrl = streamer.ProfileImageUrl,
                StreamerDisplayName = streamer.DisplayName,
                StreamerId = streamer.Id,
                Game = game.Name,
                BoxArtUrl = game.BoxArtUrl.Replace("{width}", "52").Replace("{height}", "72")
            };

            Live?.Invoke(this, twitchStream);
        }
    }
}
