﻿using Annaki.IO;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annaki.Logging
{
    public static class LoggingManager
    {
        public static void Setup()
        {
            // Make sure Log folder exists
            Directory.CreateDirectory(Path.Combine(IOUtil.AppDirectory, "Logs"));

            // Checks for existing latest log
            if (File.Exists(Path.Combine(IOUtil.AppDirectory, "Logs", "latest.log")))
            {
                // This is no longer the latest log; move to backlogs
                string oldLogFileName = File.ReadAllLines(Path.Combine(IOUtil.AppDirectory, "Logs", "latest.log"))[0];
                File.Move(Path.Combine(IOUtil.AppDirectory, "Logs", "latest.log"), Path.Combine(IOUtil.AppDirectory, "Logs", oldLogFileName));
            }

            // Builds a file name to prepare for future backlogging
            string logFileName = $"{DateTime.Now:dd-MM-yy}-1.log";

            // Loops until the log file doesn't exist
            int index = 2;
            while (File.Exists(Path.Combine(IOUtil.AppDirectory, "Logs", logFileName)))
            {
                logFileName = $"{DateTime.Now:dd-MM-yy}-{index}.log";
                index++;
            }

            // Logs the future backlog file name
            File.WriteAllText(Path.Combine(IOUtil.AppDirectory, "Logs", "latest.log"), $"{logFileName}\n");

            // Set up logging through NLog
            LoggingConfiguration config = new LoggingConfiguration();

            FileTarget logfile = new FileTarget("logfile")
            {
                FileName = Path.Combine(IOUtil.AppDirectory, "Logs", "latest.log"),
                Layout = "[${time}] [${level:uppercase=true}] [${logger}] ${message}"
            };
            config.AddRule(NLog.LogLevel.Trace, NLog.LogLevel.Fatal, logfile);


            ColoredConsoleTarget coloredConsoleTarget = new ColoredConsoleTarget
            {
                UseDefaultRowHighlightingRules = true
            };
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, coloredConsoleTarget);
            LogManager.Configuration = config;
        }
    }
}
