﻿using Annaki.Twitch.Models;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Annaki.Database
{
    public class DataProvider
    {
        public static async Task AddNotificationAsync(string streamerId, ulong guildId, ulong channelId, CancellationToken cancellationToken = default)
        {
            using SqliteConnection conn = await Sqlite.CreateAsync(cancellationToken).SafeAsync();
            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
INSERT INTO STREAMERS(
    twitch_user_id,
    guild_id,
    channel_id
) VALUES(
    $streamerId,
    $guildId,
    $channelId
)";
            cmd.Parameters.AddWithValue("$streamerId", streamerId);
            cmd.Parameters.AddWithValue("$guildId", ToDatabaseInteger(guildId));
            cmd.Parameters.AddWithValue("$channelId", ToDatabaseInteger(channelId));

            await cmd.ExecuteNonQueryAsync(cancellationToken).SafeAsync();
        }

        public static async Task<TwitchNotificationDbo[]> FetchNotificationsByStreamerIdAsync(string streamerId, CancellationToken cancellationToken = default)
        {
            using SqliteConnection conn = await Sqlite.CreateAsync(cancellationToken).SafeAsync();
            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
SELECT 
    guild_id, 
    channel_id
FROM STREAMERS
WHERE twitch_user_id = $streamerId";

            cmd.Parameters.AddWithValue("$streamerId", streamerId);

            SqliteDataReader reader = await cmd.ExecuteReaderAsync(cancellationToken).SafeAsync();

            List<TwitchNotificationDbo> result = new();
            while (await reader.ReadAsync(cancellationToken).SafeAsync())
            {
                result.Add(new TwitchNotificationDbo
                {
                    TwitchUserId = streamerId,
                    GuildId = FromDatabaseInteger(reader.GetInt64(0)),
                    ChannelId = FromDatabaseInteger(reader.GetInt64(1))
                });

            }

            return result.ToArray();
        }

        public static async Task<TwitchNotificationDbo> FetchNotificationAsync(string streamerId, ulong guildId, CancellationToken cancellationToken = default)
        {
            using SqliteConnection conn = await Sqlite.CreateAsync(cancellationToken).SafeAsync();
            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
SELECT 
    guild_id, 
    channel_id
FROM STREAMERS
WHERE twitch_user_id = $streamerId
AND guild_id = $guildId";

            cmd.Parameters.AddWithValue("$streamerId", streamerId);
            cmd.Parameters.AddWithValue("$guildId", ToDatabaseInteger(guildId));

            SqliteDataReader reader = await cmd.ExecuteReaderAsync(cancellationToken).SafeAsync();
            if (!await reader.ReadAsync(cancellationToken).SafeAsync())
            {
                return null;
            }

            TwitchNotificationDbo notificationDbo = new()
            {
                TwitchUserId = streamerId,
                GuildId = FromDatabaseInteger(reader.GetInt64(0)),
                ChannelId = FromDatabaseInteger(reader.GetInt64(1))
            };

            return notificationDbo;
        }

        public static async Task<List<string>> FetchAllStreamersAsync(CancellationToken cancellationToken = default)
        {
            using SqliteConnection conn = await Sqlite.CreateAsync(cancellationToken).SafeAsync();
            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT DISTINCT twitch_user_id FROM STREAMERS";

            SqliteDataReader reader = await cmd.ExecuteReaderAsync(cancellationToken).SafeAsync();

            List<string> streamers = new();
            while(await reader.ReadAsync(cancellationToken).SafeAsync())
            {
                streamers.Add(reader.GetString(0));
            }

            return streamers;
        }

        public static async Task<bool> RemoveNotificationsByLocationAsync(string streamerId, ulong guildId, CancellationToken cancellationToken = default)
        {
            using SqliteConnection conn = await Sqlite.CreateAsync(cancellationToken).SafeAsync();
            using SqliteCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
DELETE FROM STREAMERS
WHERE twitch_user_id = $streamerId
AND guild_id = $guildId";
            cmd.Parameters.AddWithValue("$streamerId", streamerId);
            cmd.Parameters.AddWithValue("$guildId", ToDatabaseInteger(guildId));

            return await cmd.ExecuteNonQueryAsync(cancellationToken).SafeAsync() > 0;
        }

        private static long ToDatabaseInteger(ulong value)
        {
            return unchecked((long)value + long.MinValue);
        }

        private static ulong FromDatabaseInteger(long value)
        {
            return unchecked((ulong)(value - long.MinValue));
        }
    }
}
