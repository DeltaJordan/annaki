﻿using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annaki.Discord.Commands
{
    [SlashRequireOwner]
    [SlashCommandGroup("team", "Commands related to Eventide.")]
    public class EventideModule : ApplicationCommandModule
    {
        [SlashCommand("announce", "Announce something to the team.")]
        public async Task TeamAnnounceAsync(
            InteractionContext ctx,
            [Option("hoursFromNow", "How many hours from now the announced event occurs.")] long hoursFromNow,
            [Option("message", "The announcement message to relay to the team.")] string message
        ) {
            await ctx.CreateResponseAsync("Creating announcement...").SafeAsync();
            DateTimeOffset? dateTime = DateTimeOffset.UtcNow.AddHours(hoursFromNow);
            dateTime = hoursFromNow > 0 ? dateTime.Value.Date.Add(new TimeSpan(dateTime.Value.Hour, 0, 0)) : null;

            DiscordEmbedBuilder embedBuilder = new()
            {
                Title = message
            };

            embedBuilder.WithThumbnail("https://cdn.discordapp.com/attachments/807407914272292864/1020046255658893453/Eventide_Team_Logo.png");

            if (dateTime.HasValue)
            {
                embedBuilder.AddField("Starting at or after:", $"<t:{dateTime.Value.ToUnixTimeMilliseconds() / 1000}:t>");
            }

            await ctx.Channel.SendMessageAsync("<@&738823408686727251>", embedBuilder.Build()).SafeAsync();
            await ctx.DeleteResponseAsync().SafeAsync();
        }

        [SlashCommand("avalibility", "Creates an announcement with reactions for members to indicate availability.")]
        public async Task TeamAvailabilityAsync(
            InteractionContext ctx,
            [Option("timeEntry", "Time of the event, parsed from a string.")]string timeEntry,
            [Option("message", "The message shown to the team members.")] string message
        ) {
            await ctx.CreateResponseAsync("Creating availablity post...").SafeAsync();
            if (DateTimeOffset.TryParse(timeEntry, out DateTimeOffset eventTime))
            {
                DiscordEmoji confirmationEmoji = DiscordEmoji.FromUnicode("👍");
                DiscordEmoji denyEmoji = DiscordEmoji.FromUnicode("👎");

                // TODO: This can be interactive now.
                DiscordMessage confirmationMessage = await ctx.Channel.SendMessageAsync($"Is the following timestamp correct?\n<t:{eventTime.ToUnixTimeSeconds()}:t>").SafeAsync();
                await confirmationMessage.CreateReactionAsync(confirmationEmoji).SafeAsync();
                await confirmationMessage.CreateReactionAsync(denyEmoji).SafeAsync();
                InteractivityResult<MessageReactionAddEventArgs> interactivity = await confirmationMessage.WaitForReactionAsync(ctx.Member).SafeAsync();
                if (!interactivity.TimedOut && interactivity.Result.Emoji == confirmationEmoji)
                {
                    DiscordEmbedBuilder embedBuilder = new()
                    {
                        Title = message
                    };

                    embedBuilder.WithThumbnail("https://cdn.discordapp.com/attachments/807407914272292864/1020046255658893453/Eventide_Team_Logo.png");
                    embedBuilder.AddField("Starting at or after:", $"<t:{eventTime.ToUnixTimeSeconds()}:t>");

                    DiscordMessage announcement = await ctx.Channel.SendMessageAsync("<@&738823408686727251>", embedBuilder.Build()).SafeAsync();
                    await announcement.CreateReactionAsync(DiscordEmoji.FromUnicode("👍")).SafeAsync();
                    await announcement.CreateReactionAsync(DiscordEmoji.FromUnicode("👎")).SafeAsync();
                    await ctx.DeleteResponseAsync().SafeAsync();
                }

                await confirmationMessage.DeleteAsync().SafeAsync();
            }
            else
            {
                await ctx.EditResponseAsync(new DiscordWebhookBuilder().WithContent("Unable to parse the specified DateTime.")).SafeAsync();
            }
        }

        //[Command("tourny"), Aliases("to")]
        //public async Task TournyWatcherAsync(InteractionContext ctx, string registration, [RemainingText] string description)
        //{
        //    if (DateTimeOffset.TryParse(registration, out DateTimeOffset regEnd))
        //    {
        //        DiscordEmoji confirmationEmoji = DiscordEmoji.FromUnicode("👍");
        //        DiscordEmoji denyEmoji = DiscordEmoji.FromUnicode("👎");

        //        DiscordMessage confirmationMessage = await ctx.RespondAsync($"Is the following timestamp correct?\n<t:{regEnd.ToUnixTimeSeconds()}:F>").SafeAsync();
        //        await confirmationMessage.CreateReactionAsync(confirmationEmoji).SafeAsync();
        //        await confirmationMessage.CreateReactionAsync(denyEmoji).SafeAsync();
        //        InteractivityResult<MessageReactionAddEventArgs> interactivity = await confirmationMessage.WaitForReactionAsync(ctx.Member).SafeAsync();
        //        if (!interactivity.TimedOut && interactivity.Result.Emoji == confirmationEmoji)
        //        {
        //            DiscordEmbedBuilder builder = new()
        //            {
        //                Title = $"Sign ups end at <t:{regEnd.ToUnixTimeSeconds()}:F>.",
        //                Description = description,
        //                Color = DiscordColor.Cyan
        //            };

        //            builder.WithThumbnail("https://cdn.discordapp.com/attachments/807407914272292864/1020046255658893453/Eventide_Team_Logo.png");

        //            await ctx.Channel.SendMessageAsync("<@&738823408686727251>", builder.Build()).SafeAsync();
        //            await ctx.Message.DeleteAsync().SafeAsync();
        //        }

        //        await confirmationMessage.DeleteAsync().SafeAsync();
        //    }
        //    else
        //    {
        //        await ctx.RespondAsync("Unable to parse the specified DateTime.").SafeAsync();
        //    }
        //}
    }
}
