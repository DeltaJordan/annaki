﻿using Annaki.Discord.Exceptions;
using DSharpPlus.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;

namespace Annaki.Discord.Commands
{
    //[SlashCommandGroup("youtube", "Commands related to youtube announcements.")]
    //public class YouTubeModule : ApplicationCommandModule
    //{
    //    [SlashCommand("add", "Add notifications to specified channel for new videos.")]
    //    [SlashRequireUserPermissions(DSharpPlus.Permissions.Administrator)]
    //    public async Task AddNotificationAsync(
    //        InteractionContext ctx,
    //        [Option("channel", "Discord text channel to post the new video announcement.", true)] DiscordChannel channel,
    //        [Option("channelName", "YouTube channel to watch for new videos.")] string channelName
    //    ) {
    //        try
    //        {
    //            await Program.YouTubeClient.AddNotificationAsync(username, ctx.Guild.Id, channel.Id).SafeAsync();

    //            await ctx.CreateResponseAsync($"Successfully added notifications for {username} to {channel.Mention}.", true).SafeAsync();
    //        }
    //        catch (AnnakiDuplicateException)
    //        {
    //            await ctx.CreateResponseAsync("This streamer already has notifications in this server. Duplicate notifications are not supported.", true).SafeAsync();
    //        }
    //        catch (Exception ex)
    //        {
    //            Logger.Error(ex);
    //        }
    //    }
    //}
}