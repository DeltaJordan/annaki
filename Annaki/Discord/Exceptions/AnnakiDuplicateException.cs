﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Annaki.Discord.Exceptions
{
    public class AnnakiDuplicateException : Exception
    {
        public AnnakiDuplicateException(string message) : base(message) { }
    }
}
