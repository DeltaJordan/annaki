﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Annaki.Extensions
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class TaskExtensions
    {
        public static ConfiguredTaskAwaitable SafeAsync(this Task task)
        {
            return task.ConfigureAwait(false);
        }

        public static ConfiguredTaskAwaitable<T> SafeAsync<T>(this Task<T> task)
        {
            return task.ConfigureAwait(false);
        }

#pragma warning disable AKI003
        public static ConfiguredValueTaskAwaitable SafeAsync(this ValueTask task)
        {
            return task.ConfigureAwait(false);
        }

        public static ConfiguredValueTaskAwaitable<T> SafeAsync<T>(this ValueTask<T> task)
        {
            return task.ConfigureAwait(false);
        }
#pragma warning restore AKI003

#pragma warning disable AKI002
        public static async Task<TOut> Then<TIn, TOut>(
            this Task<TIn> task,
            Func<TIn, TOut> syncFunc
        )
        {
            return syncFunc(await task.SafeAsync());
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static async Task<TOut> As<TIn, TOut>(this Task<TIn> task) where TIn : TOut
        {
            return await task.SafeAsync();
        }
#pragma warning restore AKI002

        public static async Task<TOut> ThenAsync<TIn, TOut>(
            this Task<TIn> task,
            Func<TIn, Task<TOut>> asyncFunc
        )
        {
            return await asyncFunc(await task.SafeAsync()).SafeAsync();
        }
    }
}
