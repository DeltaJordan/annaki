﻿global using Annaki.Extensions;
using Annaki.Discord;
using Annaki.Logging;
using Annaki.Security;
using Annaki.Twitch;
using TwitchLib.Client;

namespace Annaki
{
    public static class Program
    {
        public static AnnakiTwitchClient TwitchClient { get; private set; }
        public static AnnakiDiscordClient DiscordClient { get; private set; }

        public static async Task Main(string[] args)
        {
            if (args.Any(x => x.Equals("--forget", StringComparison.InvariantCultureIgnoreCase)))
            {
                ApiCredentials credentials = new("twitch");
                credentials.Forget();
                credentials = new("discord");
                credentials.Forget();
            }

            LoggingManager.Setup();

            TwitchClient = new();
            await TwitchClient.StartAsync().SafeAsync();

            DiscordClient = new();
            await DiscordClient.StartAsync().SafeAsync();

            await Task.Delay(-1).SafeAsync();
        }
    }
}