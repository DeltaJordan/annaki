﻿using Annaki.Discord.Commands.Containers;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Lavalink;
using DSharpPlus.Lavalink.Entities;
using DSharpPlus.SlashCommands;
using DSharpPlus.SlashCommands.Attributes;
using NLog;
using System.Text;

namespace Annaki.Discord.Commands
{
    [SlashCommandGroup("music", "CURRENTLY DISABLED"), SlashRequireOwner]
    public class MusicModule : ApplicationCommandModule
    {
        //private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        //private static readonly DiscordEmoji[] numberEmoji = new DiscordEmoji[5] 
        //{
        //    DiscordEmoji.FromUnicode("1️⃣"),
        //    DiscordEmoji.FromUnicode("2️⃣"),
        //    DiscordEmoji.FromUnicode("3️⃣"),
        //    DiscordEmoji.FromUnicode("4️⃣"),
        //    DiscordEmoji.FromUnicode("5️⃣")
        //};

        //[Command("join"), Aliases("j")]
        //public async Task JoinAsync(CommandContext ctx, DiscordChannel channel = null)
        //{
        //    if (channel == null)
        //    {
        //        if (ctx.Member.VoiceState?.Channel != null)
        //        {
        //            channel = ctx.Member.VoiceState.Channel;
        //        }
        //        else
        //        {
        //            await ctx.RespondAsync("Please join a channel first or specify a channel in the command.").SafeAsync();
        //            return;
        //        }
        //    }

        //    LavalinkExtension lava = ctx.Client.GetLavalink();
        //    if (!lava.ConnectedNodes.Any())
        //    {
        //        await ctx.RespondAsync("The Lavalink connection is not established").SafeAsync();
        //        return;
        //    }

        //    LavalinkNodeConnection node = lava.ConnectedNodes.Values.First();

        //    if (channel.Type != ChannelType.Voice)
        //    {
        //        await ctx.RespondAsync("Not a valid voice channel.").SafeAsync();
        //        return;
        //    }

        //    LavalinkGuildConnection conn = await node.ConnectAsync(channel).SafeAsync();

        //    conn.TrackException += Node_TrackExceptionAsync;
        //    conn.TrackStuck += Node_TrackStuckAsync;
        //    conn.PlaybackFinished += Node_PlaybackFinishedAsync;

        //    if (!MusicContainer.Queue.ContainsKey(ctx.Guild.Id))
        //    {
        //        MusicContainer.Queue[ctx.Guild.Id] = new List<LavalinkTrack>();
        //        MusicContainer.Stale[ctx.Guild.Id] = new List<LavalinkTrack>();
        //        MusicContainer.Loop[ctx.Guild.Id] = false;
        //    }

        //    await ctx.RespondAsync($"Joined {channel.Name}!").SafeAsync();
        //}

        //[Command("leave"), Aliases("l")]
        //public async Task LeaveAsync(CommandContext ctx)
        //{
        //    LavalinkGuildConnection conn = await GetContextConnectionAsync(ctx).SafeAsync();

        //    if (conn == null)
        //    {
        //        return;
        //    }

        //    await conn.DisconnectAsync().SafeAsync();

        //    conn.TrackException += Node_TrackExceptionAsync;
        //    conn.TrackStuck += Node_TrackStuckAsync;
        //    conn.PlaybackFinished += Node_PlaybackFinishedAsync;

        //    await ctx.RespondAsync($"Left {conn.Channel.Name}!").SafeAsync();
        //}

        //[Command("search"), Aliases("s")]
        //public async Task PlayAsync(CommandContext ctx, [RemainingText] string search)
        //{
        //    try
        //    {
        //        if (ctx.Member.VoiceState?.Channel == null)
        //        {
        //            await ctx.RespondAsync("You are not in a voice channel.").SafeAsync();
        //            return;
        //        }

        //        LavalinkExtension lava = ctx.Client.GetLavalink();
        //        LavalinkNodeConnection node = lava.ConnectedNodes.Values.First();
        //        LavalinkGuildConnection conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

        //        if (conn == null)
        //        {
        //            await ctx.RespondAsync("Lavalink is not connected.").SafeAsync();
        //            return;
        //        }

        //        LavalinkLoadResult loadResult = await node.Rest.GetTracksAsync(search).SafeAsync();

        //        if (loadResult.LoadResultType == LavalinkLoadResultType.LoadFailed
        //            || loadResult.LoadResultType == LavalinkLoadResultType.NoMatches)
        //        {
        //            await ctx.RespondAsync($"Track search failed for {search}.").SafeAsync();
        //            return;
        //        }

        //        LavalinkTrack selectedTrack;
        //        List<LavalinkTrack> tracks = loadResult.Tracks.ToList();
        //        if (tracks.Count == 1)
        //        {
        //            selectedTrack = tracks[0];
        //        }
        //        else
        //        {
        //            if (tracks.Count > 5)
        //            {
        //                tracks = tracks.Take(5).ToList();
        //            }

        //            DiscordEmbedBuilder searchBuilder = new()
        //            {
        //                Color = DiscordColor.Red,
        //                Title = "Results from YouTube.",
        //                Description = "Select the number of the video you want to queue:"
        //            };

        //            for (int i = 0; i < tracks.Count; i++)
        //            {
        //                LavalinkTrack track = tracks[i];
        //                searchBuilder.AddField($"{numberEmoji[i]}", $"**{track.Title}** by *{track.Author}*");
        //            }

        //            DiscordMessage searchMessage = await ctx.RespondAsync(searchBuilder.Build()).SafeAsync();

        //            for (int i = 0; i < tracks.Count; i++)
        //            {
        //                await searchMessage.CreateReactionAsync(numberEmoji[i]).SafeAsync();
        //            }

        //            InteractivityResult<MessageReactionAddEventArgs> interactivityResult = await searchMessage.WaitForReactionAsync(ctx.Member).SafeAsync();
        //            if (interactivityResult.TimedOut)
        //            {
        //                await ctx.RespondAsync("You have to be quicker than that...").SafeAsync();
        //                return;
        //            }

        //            selectedTrack = tracks[Array.FindIndex(numberEmoji, x => x.Name == interactivityResult.Result.Emoji.Name)];
        //        }

        //        MusicContainer.Queue[ctx.Guild.Id].Add(selectedTrack);

        //        if (conn.CurrentState?.CurrentTrack == null)
        //        {
        //            await conn.PlayAsync(selectedTrack).SafeAsync();
        //        }

        //        await ctx.RespondAsync($"Queued track {selectedTrack.Title} at position {MusicContainer.Queue[ctx.Guild.Id].Count}").SafeAsync();
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error(ex);
        //    }
        //}

        //[Command("skip")]
        //public async Task SkipAsync(CommandContext ctx)
        //{
        //    LavalinkGuildConnection conn = await GetContextConnectionAsync(ctx).SafeAsync();

        //    if (conn == null)
        //    {
        //        return;
        //    }

        //    List<LavalinkTrack> currentQueue = MusicContainer.Queue[conn.Guild.Id];

        //    if (MusicContainer.Loop[conn.Guild.Id])
        //    {
        //        MusicContainer.Stale[conn.Guild.Id].Add(currentQueue[0]);
        //    }

        //    currentQueue.RemoveAt(0);

        //    if (currentQueue.Count > 0)
        //    {
        //        await conn.PlayAsync(currentQueue[0]).SafeAsync();
        //    }
        //    else if (MusicContainer.Loop[conn.Guild.Id])
        //    {
        //        MusicContainer.Queue[conn.Guild.Id] = MusicContainer.Stale[conn.Guild.Id];
        //        MusicContainer.Stale[conn.Guild.Id].Clear();
        //    }
        //    else
        //    {
        //        await conn.StopAsync().SafeAsync();
        //    }
        //}

        //[Command("loop")]
        //public async Task LoopAsync(CommandContext ctx)
        //{
        //    MusicContainer.Loop[ctx.Guild.Id] = !MusicContainer.Loop[ctx.Guild.Id];

        //    await ctx.RespondAsync(MusicContainer.Loop[ctx.Guild.Id] ? "Now looping queue." : "No longer looping queue.").SafeAsync();
        //}

        //[Command("status")]
        //public async Task StatusAsync(CommandContext ctx)
        //{
        //    LavalinkGuildConnection conn = await GetContextConnectionAsync(ctx).SafeAsync();

        //    if (conn == null)
        //    {
        //        return;
        //    }

        //    DiscordEmbedBuilder builder = new();
        //    LavalinkTrack currentTrack = conn.CurrentState?.CurrentTrack;
        //    if (currentTrack != null) 
        //    {
        //        builder.WithTitle($"__**{currentTrack.Title}** by *{currentTrack.Author}*__");
        //        string positionDisplay;
        //        if (conn.CurrentState.CurrentTrack.Length.TotalMinutes > 60)
        //        {
        //            positionDisplay = $"|--------------------| [{conn.CurrentState.PlaybackPosition:hh\\:mm\\:ss}/{conn.CurrentState.CurrentTrack.Length:hh\\:mm\\:ss}]";
        //        }
        //        else
        //        {
        //            positionDisplay = $"|--------------------| [{conn.CurrentState.PlaybackPosition:mm\\:ss}/{conn.CurrentState.CurrentTrack.Length:mm\\:ss}]";
        //        }

        //        int position = (int)((conn.CurrentState.PlaybackPosition.TotalMinutes / conn.CurrentState.CurrentTrack.Length.TotalMinutes) * 20);
        //        StringBuilder sb = new(positionDisplay);
        //        sb[position + 1] = '█';
        //        positionDisplay = sb.ToString();

        //        builder.WithDescription(positionDisplay);

        //        int queueCount = MusicContainer.Queue[ctx.Guild.Id].Count - 1;
        //        if (queueCount > 0)
        //        {
        //            List<LavalinkTrack> queue = MusicContainer.Queue[ctx.Guild.Id].Skip(1).ToList();
        //            if (queueCount > 5)
        //            {
        //                for (int i = 0; i < 5; i++)
        //                {
        //                    LavalinkTrack track = queue[i];
        //                    builder.AddField($"{numberEmoji[i]}", $"**{track.Title}** by *{track.Author}*");
        //                }

        //                builder.AddField($"*and {queueCount - 5} more*", "...");
        //            }
        //            else
        //            {
        //                for (int i = 0; i < queue.Count; i++)
        //                {
        //                    LavalinkTrack track = queue[i];
        //                    builder.AddField($"{numberEmoji[i]}", $"**{track.Title}** by *{track.Author}*");
        //                }
        //            }
        //        }

        //        await ctx.RespondAsync(builder.Build()).SafeAsync();
        //    }
        //}

        //private static async Task<LavalinkGuildConnection> GetContextConnectionAsync(CommandContext ctx)
        //{
        //    LavalinkExtension lava = ctx.Client.GetLavalink();
        //    if (!lava.ConnectedNodes.Any())
        //    {
        //        await ctx.RespondAsync("The Lavalink connection is not established").SafeAsync();
        //        return null;
        //    }

        //    LavalinkNodeConnection node = lava.ConnectedNodes.Values.First();

        //    DiscordChannel channel = ctx.Member.VoiceState?.Channel;
        //    if (channel == null)
        //    {
        //        await ctx.RespondAsync("You are not connected to a voice channel.").SafeAsync();
        //        return null;
        //    }

        //    LavalinkGuildConnection conn = node.GetGuildConnection(channel.Guild);

        //    if (conn == null)
        //    {
        //        await ctx.RespondAsync("Lavalink is not connected.").SafeAsync();
        //    }

        //    return conn;
        //}

        //private async Task Node_PlaybackFinishedAsync(LavalinkGuildConnection sender, DSharpPlus.Lavalink.EventArgs.TrackFinishEventArgs e)
        //{
        //    List<LavalinkTrack> currentQueue = MusicContainer.Queue[sender.Guild.Id];
            
        //    if (currentQueue.Count == 0)
        //    {
        //        return;
        //    }

        //    if (MusicContainer.Loop[sender.Guild.Id])
        //    {
        //        MusicContainer.Stale[sender.Guild.Id].Add(currentQueue[0]);
        //    }

        //    currentQueue.RemoveAt(0);

        //    if (currentQueue.Count > 0)
        //    {
        //        await sender.PlayAsync(currentQueue[0]).SafeAsync();
        //    }
        //    else if (MusicContainer.Loop[sender.Guild.Id])
        //    {
        //        MusicContainer.Queue[sender.Guild.Id] = MusicContainer.Stale[sender.Guild.Id];
        //        MusicContainer.Stale[sender.Guild.Id].Clear();
        //    }
        //    else
        //    {
        //        await sender.StopAsync().SafeAsync();
        //    }
        //}

        //private async Task Node_TrackStuckAsync(LavalinkGuildConnection sender, DSharpPlus.Lavalink.EventArgs.TrackStuckEventArgs e)
        //{
        //    Logger.Error($"{e.Track.Title} got stuck for guild {sender.Guild.Name}.");
        //}

        //private async Task Node_TrackExceptionAsync(LavalinkGuildConnection sender, DSharpPlus.Lavalink.EventArgs.TrackExceptionEventArgs e)
        //{
        //    Logger.Error($"{e.Track.Title} encountered an exception for guild {sender.Guild.Name}.\nError: {e.Error}");
        //}
    }
}